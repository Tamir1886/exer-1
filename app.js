// const reqData=require('./config')
// import reqData from './config'
// const FIELD_KEYS = ['temp', 'feels_like', 'pressure', 'humidity', 'uvi'];

// const apiWrapper = require('./apiWrapper');

// apiWrapper.getWeatherData(34.8469023, 32.1656255, 'herzliya').then((res, err) => {

    //   console.log(JSON.stringify(res));


    
    const citiesList = [
        {
          "dt": 1629727200,
          "temp": 305.27,
          "feels_like": 312.27,
          "pressure": 1006,
          "humidity": 72,
          "dew_point": 299.58,
          "uvi": 1.98,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.77,
          "wind_deg": 246,
          "wind_gust": 5.07,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629730800,
          "temp": 306.23,
          "feels_like": 313.23,
          "pressure": 1006,
          "humidity": 71,
          "dew_point": 300.26,
          "uvi": 0.64,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.68,
          "wind_deg": 245,
          "wind_gust": 5.1,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629734400,
          "temp": 305.24,
          "feels_like": 312.24,
          "pressure": 1006,
          "humidity": 72,
          "dew_point": 299.55,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.73,
          "wind_deg": 251,
          "wind_gust": 5.27,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629738000,
          "temp": 304.23,
          "feels_like": 311.23,
          "pressure": 1006,
          "humidity": 72,
          "dew_point": 298.58,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.22,
          "wind_deg": 262,
          "wind_gust": 4.78,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629741600,
          "temp": 303.15,
          "feels_like": 309.19,
          "pressure": 1007,
          "humidity": 74,
          "dew_point": 298.01,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.75,
          "wind_deg": 276,
          "wind_gust": 4.2,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629745200,
          "temp": 302.03,
          "feels_like": 306.55,
          "pressure": 1007,
          "humidity": 75,
          "dew_point": 297.15,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.36,
          "wind_deg": 288,
          "wind_gust": 3.81,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629748800,
          "temp": 300.94,
          "feels_like": 304.05,
          "pressure": 1007,
          "humidity": 75,
          "dew_point": 296.12,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.52,
          "wind_deg": 286,
          "wind_gust": 3.12,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629752400,
          "temp": 300.89,
          "feels_like": 303.94,
          "pressure": 1006,
          "humidity": 75,
          "dew_point": 295.95,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.11,
          "wind_deg": 278,
          "wind_gust": 2.63,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629756000,
          "temp": 300.83,
          "feels_like": 303.81,
          "pressure": 1006,
          "humidity": 75,
          "dew_point": 295.94,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.34,
          "wind_deg": 268,
          "wind_gust": 2.74,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629759600,
          "temp": 300.85,
          "feels_like": 303.86,
          "pressure": 1006,
          "humidity": 75,
          "dew_point": 295.93,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.87,
          "wind_deg": 257,
          "wind_gust": 3.1,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629763200,
          "temp": 300.82,
          "feels_like": 303.79,
          "pressure": 1006,
          "humidity": 75,
          "dew_point": 296.04,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.44,
          "wind_deg": 251,
          "wind_gust": 3.63,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629766800,
          "temp": 300.76,
          "feels_like": 303.79,
          "pressure": 1006,
          "humidity": 76,
          "dew_point": 296.08,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.48,
          "wind_deg": 250,
          "wind_gust": 3.6,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629770400,
          "temp": 300.74,
          "feels_like": 303.75,
          "pressure": 1006,
          "humidity": 76,
          "dew_point": 296.11,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.94,
          "wind_deg": 252,
          "wind_gust": 2.97,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629774000,
          "temp": 300.64,
          "feels_like": 303.65,
          "pressure": 1006,
          "humidity": 77,
          "dew_point": 296.16,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.76,
          "wind_deg": 261,
          "wind_gust": 2.73,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629777600,
          "temp": 300.64,
          "feels_like": 303.65,
          "pressure": 1006,
          "humidity": 77,
          "dew_point": 296.13,
          "uvi": 0.18,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.24,
          "wind_deg": 264,
          "wind_gust": 2.25,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629781200,
          "temp": 300.73,
          "feels_like": 303.72,
          "pressure": 1006,
          "humidity": 76,
          "dew_point": 296.05,
          "uvi": 0.9,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.04,
          "wind_deg": 246,
          "wind_gust": 1.96,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629784800,
          "temp": 300.84,
          "feels_like": 303.84,
          "pressure": 1007,
          "humidity": 75,
          "dew_point": 296.09,
          "uvi": 2.51,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.22,
          "wind_deg": 235,
          "wind_gust": 2.02,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629788400,
          "temp": 300.96,
          "feels_like": 304.09,
          "pressure": 1007,
          "humidity": 75,
          "dew_point": 296.15,
          "uvi": 4.86,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.58,
          "wind_deg": 228,
          "wind_gust": 2.36,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629792000,
          "temp": 301.09,
          "feels_like": 304.37,
          "pressure": 1008,
          "humidity": 75,
          "dew_point": 296.27,
          "uvi": 7.32,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.93,
          "wind_deg": 220,
          "wind_gust": 2.75,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629795600,
          "temp": 301.21,
          "feels_like": 304.64,
          "pressure": 1008,
          "humidity": 75,
          "dew_point": 296.38,
          "uvi": 9.12,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.29,
          "wind_deg": 210,
          "wind_gust": 3.08,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629799200,
          "temp": 301.34,
          "feels_like": 304.93,
          "pressure": 1008,
          "humidity": 75,
          "dew_point": 296.48,
          "uvi": 9.68,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.78,
          "wind_deg": 205,
          "wind_gust": 3.67,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629802800,
          "temp": 301.43,
          "feels_like": 305.13,
          "pressure": 1008,
          "humidity": 75,
          "dew_point": 296.48,
          "uvi": 8.76,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.91,
          "wind_deg": 211,
          "wind_gust": 3.86,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629806400,
          "temp": 301.45,
          "feels_like": 305.18,
          "pressure": 1008,
          "humidity": 75,
          "dew_point": 296.54,
          "uvi": 6.71,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.82,
          "wind_deg": 217,
          "wind_gust": 3.84,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629810000,
          "temp": 301.52,
          "feels_like": 305.34,
          "pressure": 1007,
          "humidity": 75,
          "dew_point": 296.55,
          "uvi": 4.2,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.63,
          "wind_deg": 224,
          "wind_gust": 3.74,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629813600,
          "temp": 301.53,
          "feels_like": 305.36,
          "pressure": 1007,
          "humidity": 75, 
          "dew_point": 296.65,
          "uvi": 2.02,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.56,
          "wind_deg": 229,
          "wind_gust": 3.72,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629817200,
          "temp": 301.5,
          "feels_like": 305.46,
          "pressure": 1007,
          "humidity": 76,
          "dew_point": 296.81,
          "uvi": 0.64,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.77,
          "wind_deg": 230,
          "wind_gust": 4,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629820800,
          "temp": 301.34,
          "feels_like": 305.4,
          "pressure": 1007,
          "humidity": 78,
          "dew_point": 297.11,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.89,
          "wind_deg": 227,
          "wind_gust": 4.15,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629824400,
          "temp": 301.14,
          "feels_like": 305.07,
          "pressure": 1008,
          "humidity": 79,
          "dew_point": 297.16,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.49,
          "wind_deg": 232,
          "wind_gust": 3.67,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629828000,
          "temp": 301,
          "feels_like": 304.88,
          "pressure": 1008,
          "humidity": 80,
          "dew_point": 297.17,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.96,
          "wind_deg": 250,
          "wind_gust": 3.22,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629831600,
          "temp": 300.87,
          "feels_like": 304.7,
          "pressure": 1009,
          "humidity": 81,
          "dew_point": 297.2,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.82,
          "wind_deg": 252,
          "wind_gust": 3.07,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629835200,
          "temp": 300.7,
          "feels_like": 304.28,
          "pressure": 1009,
          "humidity": 81,
          "dew_point": 297.15,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.81,
          "wind_deg": 253,
          "wind_gust": 3.06,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629838800,
          "temp": 300.54,
          "feels_like": 304.01,
          "pressure": 1009,
          "humidity": 82,
          "dew_point": 297.13,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.81,
          "wind_deg": 254,
          "wind_gust": 3.09,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629842400,
          "temp": 300.43,
          "feels_like": 303.75,
          "pressure": 1008,
          "humidity": 82,
          "dew_point": 297.04,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.79,
          "wind_deg": 242,
          "wind_gust": 3.06,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629846000,
          "temp": 300.34,
          "feels_like": 303.53,
          "pressure": 1008,
          "humidity": 82,
          "dew_point": 296.98,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.9,
          "wind_deg": 240,
          "wind_gust": 3.09,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629849600,
          "temp": 300.26,
          "feels_like": 303.34,
          "pressure": 1008,
          "humidity": 82,
          "dew_point": 296.93,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.87,
          "wind_deg": 241,
          "wind_gust": 3.04,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629853200,
          "temp": 300.24,
          "feels_like": 303.29,
          "pressure": 1008,
          "humidity": 82,
          "dew_point": 296.86,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.45,
          "wind_deg": 244,
          "wind_gust": 2.62,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629856800,
          "temp": 300.14,
          "feels_like": 302.96,
          "pressure": 1008,
          "humidity": 81,
          "dew_point": 296.68,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.05,
          "wind_deg": 238,
          "wind_gust": 2.2,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629860400,
          "temp": 300.05,
          "feels_like": 302.76,
          "pressure": 1008,
          "humidity": 81,
          "dew_point": 296.56,
          "uvi": 0,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 1.92,
          "wind_deg": 226,
          "wind_gust": 2.01,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01n"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629864000,
          "temp": 300.1,
          "feels_like": 302.87,
          "pressure": 1008,
          "humidity": 81,
          "dew_point": 296.54,
          "uvi": 0.18,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 1.99,
          "wind_deg": 226,
          "wind_gust": 2.03,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629867600,
          "temp": 300.15,
          "feels_like": 302.89,
          "pressure": 1009,
          "humidity": 80,
          "dew_point": 296.47,
          "uvi": 0.9,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.01,
          "wind_deg": 215,
          "wind_gust": 2.02,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629871200,
          "temp": 300.3,
          "feels_like": 303.23,
          "pressure": 1009,
          "humidity": 80,
          "dew_point": 296.49,
          "uvi": 2.51,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 2.48,
          "wind_deg": 202,
          "wind_gust": 2.43,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629874800,
          "temp": 300.49,
          "feels_like": 303.55,
          "pressure": 1009,
          "humidity": 79,
          "dew_point": 296.6,
          "uvi": 4.8,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.15,
          "wind_deg": 197,
          "wind_gust": 3.09,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629878400,
          "temp": 300.62,
          "feels_like": 303.84,
          "pressure": 1010,
          "humidity": 79,
          "dew_point": 296.58,
          "uvi": 7.24,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.39,
          "wind_deg": 200,
          "wind_gust": 3.31,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629882000,
          "temp": 300.79,
          "feels_like": 304.11,
          "pressure": 1010,
          "humidity": 78,
          "dew_point": 296.63,
          "uvi": 9.02,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.81,
          "wind_deg": 201,
          "wind_gust": 3.79,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629885600,
          "temp": 300.88,
          "feels_like": 304.31,
          "pressure": 1009,
          "humidity": 78,
          "dew_point": 296.68,
          "uvi": 9.7,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.14,
          "wind_deg": 210,
          "wind_gust": 4.12,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629889200,
          "temp": 300.98,
          "feels_like": 304.54,
          "pressure": 1009,
          "humidity": 78,
          "dew_point": 296.69,
          "uvi": 8.77,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 4.25,
          "wind_deg": 218,
          "wind_gust": 4.31,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629892800,
          "temp": 301.1,
          "feels_like": 304.68,
          "pressure": 1009,
          "humidity": 77,
          "dew_point": 296.7,
          "uvi": 6.71,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.99,
          "wind_deg": 227,
          "wind_gust": 4.08,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        },
        {
          "dt": 1629896400,
          "temp": 301.21,
          "feels_like": 304.79,
          "pressure": 1008,
          "humidity": 76,
          "dew_point": 296.59,
          "uvi": 4.19,
          "clouds": 0,
          "visibility": 10000,
          "wind_speed": 3.42,
          "wind_deg": 238,
          "wind_gust": 3.62,
          "weather": [
            {
              "id": 800,
              "main": "Clear",
              "description": "clear sky",
              "icon": "01d"
            }
          ],
          "pop": 0
        }
      ];
    //   console.log(citiesList);


    const reqData=["temp","feels_like","pressure","humidity","uvi"];
    // let calculate_data = {
    //     'temp': [],
    //    'feels_likes': [],
    //     'pressure': [],
    //     "humidity": [],
    //     'uvi': []
    // }

    reqData.map((key, i) => {        
      let min=0, max=0, avg=0, counter=0, isfirstrun=true
      let temp_var = reqData[i]

      citiesList.map((item,i)=>{
        if(isfirstrun){ 
            min =item[temp_var]; 
            isfirstrun=false;
        }
        if (item[temp_var] < min) min=item[temp_var]
        if (item[temp_var] > max) max=item[temp_var]
        avg += item[temp_var]
        counter++
      })
      let averge=avg / counter
      console.log(`${temp_var} details :
      max: ${max} 
      min: ${min} 
      averge: ${averge.toFixed(1)}`)
      
      

      





        // calculate_data.temp[i] = key.temp
        // calculate_data.feels_likes[i] = key.feels_like
        // calculate_data.pressure[i] = key.pressure
        // calculate_data.humidity.push(key.humidity)
        // calculate_data.uvi[i] = key.uvi
    });
    // console.log(Math.min(...calculate_data.humidity))

    // console.log(`${Math.min(calculate_data.feels_likes)}`)


    // console.log(calculate_data)
    // for (let key in calculate_data) {
    // for (let i = 0; i < 5; i++){
        // let key =Object.keys(calculate_data)[i]
        // console.log(`${key} data is :
        //  max ${Math.max(...calculate_data[key])} 
        //  min ${Math.min(...calculate_data[key]) } 
        //  sum ${calculate_data[key].reduce((a, b) => a + b, 0)/calculate_data[key].length}`)
    // }

    // console.log(`lst_temp: ${lst_temp}`)


    // console.log(`lst_feels_like${lst_feels_like}`)
    // console.log(lst)
    // console.log(lst)
    // console.log(lst)

    //   );
// }).catch((err) => {
//     console.log(err);
// });